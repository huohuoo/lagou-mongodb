package com.lagou.edu.resume;

import com.lagou.edu.resume.snapshot.LagouResumeSnapshotApplication;
import com.lagou.edu.resume.snapshot.entity.ResumeSnapshot;
import com.lagou.edu.resume.snapshot.repository.LagouResumeSnapshotRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;


@SpringBootTest(classes = {LagouResumeSnapshotApplication.class})
@RunWith(SpringRunner.class)
public class ResumeSnopshotTest {

    @Autowired
    private LagouResumeSnapshotRepository resumeSnapshotRepository;

    /**
     * 保存简历快照测试
     */
    @Test
    public void saveResume(){
        ResumeSnapshot resume = new ResumeSnapshot();
        resume.setResumeid(12L);
        resume.setCity("tw");
        resume.setName("周杰伦");
        resume.setBirthday(new Date(System.currentTimeMillis()-(365*20*24*60*60*1000L)));
        resume.setExpectSalary(100000);
        ResumeSnapshot resumeSnapshot = resumeSnapshotRepository.save(resume);

        System.out.println("After save to mongo :"+resumeSnapshot);
    }

    /**
     * 根据resumeid获取简历快照测试
     */
    @Test
    public void getResumeByResumeid(){
        Long resumeid = 12L;
        ResumeSnapshot resumeSnapshot = resumeSnapshotRepository.findByResumeidEquals(resumeid);
        System.out.println(resumeSnapshot);
    }

    /**
     * 根据resumeid删除简历测试
     */
    @Test
    public void removeResumeById(){
        Long resumeid = 12L;
        resumeSnapshotRepository.deleteByResumeidEquals(12L);
    }

    /**
     * 查询所有简历快照测试
     */
    @Test
    public void findAll(){
        List<ResumeSnapshot> snapshotList = resumeSnapshotRepository.findAll();
        snapshotList.forEach((snapshot)->{
            System.out.println(snapshot);
        });
    }

}

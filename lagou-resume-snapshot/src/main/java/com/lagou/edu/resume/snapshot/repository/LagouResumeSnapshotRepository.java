package com.lagou.edu.resume.snapshot.repository;

import com.lagou.edu.resume.snapshot.entity.ResumeSnapshot;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LagouResumeSnapshotRepository extends MongoRepository<ResumeSnapshot,String> {

    /**
     * 根据ResumeId查找resume
     * @param resumeid
     * @return
     */
    ResumeSnapshot findByResumeidEquals(Long resumeid);


    /**
     * 删除简历
     * @param resumeid
     */
    void deleteByResumeidEquals(Long resumeid);

}
